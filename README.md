# Apuntes de electrónica: Condensador y Bobina en alterna

Hace ya tiempo de mi última entrada de apuntes de electrónica. Fue la entrada sobre el multímetro y después empecé con las entradas de análisis. Me gustaría retomar un poco las entradas de apuntes, aunque signifique retrasar otras. Ya os hablé de los condensadores y de las bobinas, también hice un análisis matemático de su comportamiento en continua con los circuitos RC y RL. Hoy analizaré matemáticamente el comportamiento de los condensadores y de las bobinas en corriente alterna.

Partiremos de tres ecuaciones: respuesta del condensador (1), respuesta de la bobina (2) y señal senoidal (3).

```math
\tag{1} i = C {dv \over dt}
```

```math
\tag{2} v = L {di \over dt} \\
```

```math
\tag{3} v = V_P \sin(\omega t) \\
```

Vamos a comenzar resolviendo la derivada de la tensión con respecto del tiempo (dv/dt):

```math
{dv \over dt} = {d[V_P \sin(\omega t)] \over dt} \\\mathstrut\\
{dv \over dt} = \omega \cdot V_P \cos(\omega t) \\\mathstrut\\
{dv \over dt} = j \omega \cdot  V_P \sin(\omega t) \\\mathstrut\\
\tag{4} {dv \over dt} = j \omega v
```

Para los que como yo tienen que tirar de tablas de primitivas para derivar os pongo las primitivas necesarias para obtener el resultado anterior:

```math
\begin{array}{ll}
   {y = k \operatorname{f}(x)} & {y' = k \operatorname{f}'(x)} \\
   {y = \sin(kx)} & {y' = k \cos(kx)} \\
\end{array}
```

También he aplicado una transformación típica en electrónica/electricidad con fasores. Tenemos una señal cosenoidal que podemos considerar como una señal senoidal adelantada 90º. Los fasores se pueden representar como coordenadas polares o como números complejos, que es como normalmente se opera. En este caso la j se utiliza en lugar de la i de la unidad imaginaria (i2 = -1) para evitar la confusiones con la i que representa la corriente eléctrica.

Ahora calculemos la reactancia capacitiva (XC):

```math
v = X_C \cdot i \\\mathstrut\\
v = X_C \cdot C {dv \over dt} \\\mathstrut\\
v = X_C \cdot C j \omega v \\\mathstrut\\
\tag{5} \boxed{X_C = {1 \over {j \omega C}}}
```

Calculemos ahora la reactancia inductiva (XL):

```math
v = X_L \cdot i \\\mathstrut\\
{dv \over dt} = X_L \cdot {di \over dt} \\\mathstrut\\
v = L {di \over dt} \\\mathstrut\\
{di \over dt} = {v \over L} \\\mathstrut\\
j \omega v = X_L \cdot {v \over L} \\\mathstrut\\
\tag{6} \boxed{X_L = j \omega L}
```

Y ya está analizada matemáticamente la respuesta del condensador y de la bobina en corriente alterna. Cuando estuve preparando esta entrada y me puse a emborronar papeles me di cuenta que aunque no era nada muy complejo si que era un tanto enrevesado dar con la solución. Al final descubrí que si calculaba primero dv/dt todo se volvía trivial pues es lo único que realmente hay que derivar, el resto son transformaciones y sustituciones más bien sencillas.

Volviendo atrás cuando calculaba  dv/dt (4) realizaba una transformación de señal cosenoidal a señal senoidal. Si no hubiera hecho eso me habría encontrado en un momento dado con la siguiente expresión:

```math
{\sin(\omega t) \over \cos(\omega t)} = \tan(\omega t)
```

Lo primero que nos damos cuenta es que no eliminamos de la expresión el tiempo, por lo que la reactancia variaría en función del tiempo (t) y sabemos que no es así, varía en función de la frecuencia (ω). Lo segundo es que sabemos la la tangente es una función con puntos indeterminados que tienden a infinito, y esto tampoco es cierto porque la reactancia siempre tiene un valor concreto.

Quiero terminar esta entrada poniendo una vez más las ecuaciones que definen a los condensadores y a las bobinas, es algo que siempre me maravillará:

```math
\begin{array}{ll}
\boxed{i = C {dv \over dt}} & \boxed{v = L {di \over dt}} \\\\
\boxed{v = V_0 \cdot e^{-t / \tau}} & \boxed{i = I_0 \cdot e^{-t / \tau}} \\\\
\tau =RC & \tau = L/R\\\\
\boxed{X_C = {1 \over {j \omega C}}} & \boxed{X_L = j \omega L} \\
\end{array}
```
